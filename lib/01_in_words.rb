class Fixnum


  def in_words
    word = {
      10**18 => "quintillion", 10**15 => "quadrillion", 10**12 => "trillion",
      10**9 => "billion", 10**6 => "million", 1000 => "thousand", 100 => "hundred",
      90 => "ninety", 80 => "eighty", 70 => "seventy", 60 => "sixty", 50 => "fifty",
      40 => "forty", 30 => "thirty", 20 => "twenty", 19=>"nineteen",
      18=>"eighteen", 17=>"seventeen", 16=>"sixteen", 15=>"fifteen",
      14=>"fourteen", 13=>"thirteen", 12=>"twelve", 11 => "eleven", 10 => "ten",
      9 => "nine", 8 => "eight", 7 => "seven", 6 => "six", 5 => "five",
      4 => "four", 3 => "three", 2 => "two", 1 => "one", 0 => "zero"
    }

    if self < 10
      word[self]
    elsif self < 20
      word[self]
    elsif self < 100
      single = self % 10
      double = self / 10 * 10
      if single != 0
        "#{word[double]}" + " " + single.in_words
      else
        "#{word[double]}"
      end
    else
      pow_to_10 = 10**(self.to_s.length - 1)
      large_key_word = large_num_word(word, pow_to_10)
      first_part = self / large_key_word

      if self % pow_to_10 != 0
        (first_part).in_words + " " + "#{word[large_key_word]} " + (self % large_key_word).in_words
      else
        (first_part).in_words + " " + "#{word[large_key_word]}"
      end
    end
  end

  def large_num_word(hash, pow_to_10)
    hash.keys.select { |key| key <= pow_to_10 }.first
  end

end
